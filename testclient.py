#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = "Bernd Zeimetz"
__contact__ = "bzed@debian.org"
__license__ = """
Copyright (C) 2010-2012 Bernd Zeimetz <bzed@debian.org>
Copyright (C) 2012 Bernd Zeimetz <b.zeimetz@conova.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import xmlrpclib
server = xmlrpclib.ServerProxy('http://localhost:5000/')
#for i in range(5):
#    print server.generate_password(40)

from monkeystore import web

import gnupg
gpg=gnupg.GPG()

def __encrypt_data__(data, keys, passphrase=None, symmetric=False):
    gpg_data = gpg.encrypt(data, keys, passphrase=passphrase, symmetric=symmetric, always_trust=True)
    if not gpg_data.ok:
        raise Exception("Failed to encrypt: \n%s" %(gpg_data.stderr,))
    return gpg_data.data

def __decrypt_data__(gpg_data, passphrase=None):
    data = gpg.decrypt(gpg_data, passphrase=passphrase, always_trust=True)
    if not data.ok:
        raise Exception("Failed to encrypt: \n%s" %(data.stderr,))
    return data.data

token=__decrypt_data__(server.retrieve_token('zebe'))
print server.list_categories('zebe', token)



token=__decrypt_data__(server.retrieve_token('zebe'))
print "1 " + token
password_crypt=__encrypt_data__('f00bar!', None, passphrase=token, symmetric=True)
print password_crypt
description_crypt=__encrypt_data__('das ist eine beschreibung!', None, passphrase=token, symmetric=True)
print description_crypt
print server.add_password('zebe', 'linux', 'zebe001', 'ssh', 'root',
                              password_crypt, description_crypt)



token=__decrypt_data__(server.retrieve_token('zebe'))
print "1 " + token
password_crypt=__encrypt_data__('f00bar!', None, passphrase=token, symmetric=True)
print password_crypt
description_crypt=__encrypt_data__('das ist eine beschreibung!', None, passphrase=token, symmetric=True)
print description_crypt
print server.add_password('zebe', 'linux', 'zebe001', 'sshasd', 'root',
                              password_crypt, description_crypt)

print __decrypt_data__(server.get_url('zebe', 'linux', 'zebe001', 'sshasd', 'root'))
token=__decrypt_data__(server.retrieve_token('zebe'))
server.delete_password('zebe', token, 'linux', 'zebe001', 'ssh', 'root')

