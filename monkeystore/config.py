DEBUG = 1

# FIXME !!!!!!
PASSWORD_DIRECTORY = '/tmp/monkeystore'
SHELVE_FILENAME = 'monkeystore.shelve'

KEYS = {
    'zebe' : '084EEAD2', # Bernd Zeimetz
    'bzed' : '084EEAD2', # Bernd Zeimetz - alias 
}

acl_windows = ('zebe', 'bzed' )
acl_bereitschaft = ('zebe', 'bzed')
acl_linux = ('zebe', 'bzed')
acl_network = ('zebe', 'bzed')

ACL_METADATA = ('zebe','bzed')

CATEGORY_USERS = {
    'windows' : set( acl_windows + acl_bereitschaft ),
    'linux' : set( acl_linux + acl_bereitschaft ),
    'network' : set( acl_network + acl_bereitschaft )
}

LOGGER_NAME = 'monkeystore'
LOGGER_LOCATION = '/tmp/monkeystore.log'
DEBUG = False

