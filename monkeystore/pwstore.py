# -*- coding: utf-8 -*-
__author__ = "Bernd Zeimetz"
__contact__ = "bzed@debian.org"
__license__ = """
Copyright (C) 2010 Bernd Zeimetz <bzed@debian.org>
Copyright (C) 2012 Bernd Zeimetz <b.zeimetz@conova.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import os
import re
import pwd
import glob
import tempfile
from git import Repo
import gnupg
import time
import random
import urlparse
import shutil

import traceback

try:
    import simplejson as json
except ImportError:
    import json


from .urlencoder import decode_url, encode_url

class GPGpwstoreException(Exception):
    pass

#class GPGpwstoreData(object):
#    def __init__(self, hostname, service, username, password_gpg, description):
#        for i in ["hostname", "service", "username", "password_gpg"]:
#            if not locals()[i]:
#                raise GPGpwstoreException("Failed to store password: %s must be set (not %s)!" %
#                                          (i, str(locals()[i])))
#            setattr(self, i, locals()[i])
#        for i in ["description"]:
#            if locals()[i]:
#                setattr(self, i, locals()[i])
#            else:
#                setattr(self, i, "")

class GPGpwstore(object):

    def __init__(self, config):
        self.gpg_keys = config['KEYS']
        self.users = self.gpg_keys.keys()
        self.pw_directory = config['PASSWORD_DIRECTORY']
        self.debug = config['DEBUG']
        self.category_users = config['CATEGORY_USERS']
        self.metadata_access = config['ACL_METADATA']
        self.categories = self.category_users.keys()
        self.commit_messages = []
        self.__init_directories()
        self.gpg = gnupg.GPG(gnupghome=pwd.getpwuid(os.geteuid())[5] + os.path.sep + '.gnupg')
        self.url_re=re.compile(r'^([0-9a-zA-Z]+)$')

    def __init_directories(self):
        for p in [os.path.join(self.pw_directory, c) for c in self.categories]:
            if not os.path.exists(p):
                try:
                    os.makedirs(p,0700)
                except Exception, e:
                    raise GPGpwstoreException("Failed to create directory %s: %s\n%s" %(p, str(e), traceback.format_exc()))
            else:
                if not os.path.isdir(p):
                    raise GPGpwstoreException("%s exists but it is not a directory" %(p,))
        if not os.path.exists(os.path.join(self.pw_directory, '.git')):
            self.repository = Repo.init(self.pw_directory)
        else:
            self.repository = Repo(self.pw_directory)
        if self.repository.untracked_files or self.repository.is_dirty():
            self.commit()


    def __get_directory__(self,category, hostname, service, username):
        for i in ["category", "hostname", "service", "username"]:
            if not locals()[i]:
                raise GPGpwstoreException("Failed to retrieve/set password: %s must be set (not %s)!" %
                                          (i, str(locals()[i])))
        if not category in self.categories:
            raise GPGpwstoreException("Unknown category: %s" %(category,))

        return os.path.join(self.pw_directory,
                            category,
                            hostname.strip().encode('hex'),
                            service.strip().encode('hex'),
                            username.strip().encode('hex')
                           )


    def __encrypt_data__(self,data, keys, passphrase=None, symmetric=False):
        gpg_data = self.gpg.encrypt(data, keys, passphrase=passphrase, symmetric=symmetric, always_trust=True)
        if not gpg_data.ok:
            raise GPGpwstoreException("Failed to encrypt: \n%s" %(gpg_data.stderr,))
        return gpg_data.data

    def __decrypt_data__(self,gpg_data, passphrase=None):
        data = self.gpg.decrypt(gpg_data, passphrase=passphrase, always_trust=True)
        if not data.ok:
            raise GPGpwstoreException("Failed to encrypt: \n%s" %(data.stderr,))
        return data.data
        
    def __crypt_password__(self,category, password):
        try:
            users = self.category_users[category]
            keys = []
            for x in users:
                if self.gpg_keys.has_key(x):
                    keys.append(self.gpg_keys[x])
                else:
                    raise GPGpwstoreException("User %s in category %s - but no key found!" %(x, category))
        except KeyError:
            raise GPGpwstoreException("Category %s not known!" %(category,))
        except Exception, e:
            raise GPGpwstoreException("%s\n%s" %(str(e), traceback.format_exc()))
        return self.__encrypt_data__(password, keys)

    def __write_password__(self,category, hostname, service, username, password):
        data = self.__crypt_password__(category, password)
        self.__write_file__(category, hostname, service, username, data, 'password.gpg')

    def __write_description__(self,category, hostname, service, username, data):
        self.__write_file__(category, hostname, service, username, data, 'description')

    def __write_file__(self,category, hostname, service, username, data, filename):
        pwdir = self.__get_directory__(category, hostname, service, username)
        if not os.path.exists(pwdir):
            os.makedirs(pwdir)
        filename = os.path.join(pwdir, filename)
        with open(filename, 'w') as f:
            f.write(data)

    def __read_password_gpg__(self,category, hostname, service, username):
        pwfile = os.path.join(
            self.__get_directory__(category, hostname, service, username),
            'password.gpg')
        with open(pwfile, 'r') as f:
            data=f.read()
        return data


    def __get_metadata__(self):
        metadata={}
        for description_file in glob.glob(os.path.join(self.pw_directory, '*/*/*/*/description')):
            data=description_file.replace(self.pw_directory, '')
            data=data.replace('/description','')
            data=data.strip('/')
            category,hostname,service,username = data.split('/')
            hostname=hostname.decode('hex')
            service=service.decode('hex')
            username=username.decode('hex')

            if not metadata.has_key(category):
                metadata[category]={}
            if not metadata[category].has_key(hostname):
                metadata[category][hostname] = {}
            if not metadata[category][hostname].has_key(service):
                metadata[category][hostname][service] = {}
            with open(description_file, 'r') as f:
                description=f.read()
            metadata[category][hostname][service][username]=description.strip()
        return metadata



    def __get_data_by_url__(self,url):
        # find a short url file
        url=urlparse.urlparse(url).netloc
        urls=self.url_re.findall(url)
        if not (len(urls) == 1 and urls[0] == url):
            return None
        for url_file in glob.glob(os.path.join(self.pw_directory, '*/*/*/*/%s.url' %(url,))):
            data=url_file.replace(self.pw_directory, '')
            data=data.replace('/%s.url' %(url,),'')
            data=data.strip('/')
            category,hostname,service,username = data.split('/')
            hostname=hostname.decode('hex')
            service=service.decode('hex')
            username=username.decode('hex')
            return (category, hostname, service, username)
        return None

    def __check_access__(self,pwstore_user, category, raise_fault=True):
        if category not in self.categories:
            if raise_fault:
                raise GPGpwstoreException("Unknown category: %s" %(category,))
            return False
        if pwstore_user not in self.category_users[category]:
            if raise_fault:
                raise GPGpwstoreException("No access to category: %s" %(category,))
            return False
        return True

    def __check_metadata_access__(self, pwstore_user):
        if pwstore_user not in self.metadata_access:
            raise GPGpwstoreException("No access to metadata info!")

    def __get_url__(self, category, hostname, service, username):
        pwdir = self.__get_directory__(category, hostname, service, username)
        if not os.path.isdir(pwdir):
            raise GPGpwstoreException("No password found!")
        for url_file in glob.glob(pwdir + os.path.sep + '*.url'):
            url = 'monkeystore://' + os.path.basename(url_file).replace('.url','')
            return url
        short_url=encode_url((int(time.time()*10)*1000)+random.randint(0,999))
        self.__write_file__(category, hostname, service, username, '', '%s.url' %(short_url,))
        url = 'monkeystore://' + short_url
        return url


    # search for a hostname
    def search(self,pwstore_user, token, search_string_crypt, category=None):
        search_string = self.__decrypt_with_token__(search_string_crypt, token)
        metadata = self.__get_metadata__()
        if not metadata:
            return {}

        ret = {} 
        if category:
            # old API support
            self.__check_access__(pwstore_user, category)
            if not metadata.has_key(category):
                return {}
            hosts = metadata[category]
            for i in hosts.keys():
                if ((i.lower() == search_string.lower()) or
                    (len(search_string) >= 5 and i.lower().find(search_string.lower()) >=0)):
                    ret[i] = hosts[i] 
        else:
            categories=metadata.keys()
            for category in categories:
                if not self.__check_access__(pwstore_user, category, False):
                    continue
                hosts = metadata[category]
                for i in hosts.keys():
                    if ((i.lower() == search_string.lower()) or
                        (len(search_string) >= 5 and i.lower().find(search_string.lower()) >=0)):
                        if not ret.has_key(category):
                            ret[category]={}
                        ret[category][i] = hosts[i]
        return ( search_string, ret )


    def delete_password(self, pwstore_user, category, hostname, service,
                        username):
        self.__check_access__(pwstore_user, category)
        directory = self.__get_directory__(category, hostname, service, username)
        if not os.path.isfile(os.path.join(directory, 'description')):
            raise GPGpwstoreException("No password stored for %s/%s/%s/%s - can't delete." %(
                category, hostname, service,username))

        shutil.rmtree(directory)
        for p in ('..', '..'):
            directory = os.path.realpath(os.path.join(directory,p))
            # if directory is not empty, don't delete it
            # (and don't try to delete the parent...)
            if os.stat(directory).st_nlink > 2:
                break
            else:
                shutil.rmtree(directory)
        self.commit_messages.append('DELETE')
        self.commit_messages.append('USER %s' %(pwstore_user,))
        self.commit_messages.append('')
        self.commit_messages.append('%s/%s/%s/%s' %(category, hostname, service,username))
        self.commit()


    def add_password(self, pwstore_user, token, category, hostname, service,
                        username, password_crypt, description_crypt):
        self.__check_access__(pwstore_user, category)
        directory = self.__get_directory__(category, hostname, service, username)
        if os.path.isfile(os.path.join(directory, 'description')):
            raise GPGpwstoreException("Password already stored for %s/%s/%s/%s - can't add." %(
                category, hostname, service,username))
        password = self.__decrypt_with_token__(password_crypt, token)
        self.__write_password__(category, hostname, service, username, password)
        del password
        description = self.__decrypt_with_token__(description_crypt, token)
        self.__write_description__(category, hostname, service, username, description)
        url = self.__get_url__(category, hostname, service, username)
        self.commit_messages.append('ADD')
        self.commit_messages.append('USER %s' %(pwstore_user,))
        self.commit()
        return url

    def update_password(self, pwstore_user, token, category, hostname, service,
                        username, password_crypt, description_crypt=None, reencrypt=False):
        self.__check_access__(pwstore_user, category)
        directory = self.__get_directory__(category, hostname, service, username)
        if not os.path.isfile(os.path.join(directory, 'description')):
            raise GPGpwstoreException("No password stored for %s/%s/%s/%s - can't update." %(
                category, hostname, service,username))
        if password_crypt:
            password = self.__decrypt_with_token__(password_crypt, token)
            self.__write_password__(category, hostname, service, username, password)
            if not reencrypt:
                self.commit_messages.append('UPDATE_PASSWORD')
            else:
                self.commit_messages.append('REENCRYPT')
            del password
        if description_crypt:
            self.commit_messages.append('UPDATE_DESCRIPTION')
            description = self.__decrypt_with_token__(description_crypt, token)
            self.__write_description__(category, hostname, service, username, description)
        if password_crypt or description_crypt:
            self.commit_messages.append('USER %s' %(pwstore_user,))
        url = self.__get_url__(category, hostname, service, username)
        self.commit()
        return url


    def get_password(self, pwstore_user, category, hostname, service, username):
        self.__check_access__(pwstore_user, category)
        return self.__read_password_gpg__(category, hostname, service, username)

    def get_url(self, pwstore_user, category, hostname, service, username):
        self.__check_access__(pwstore_user, category)
        url = self.__get_url__(category, hostname, service, username)
        return self.crypt_data(pwstore_user, url)


    def get_password_by_url(self, pwstore_user, url):
        data = self.__get_data_by_url__(url)
        if not data:
            raise GPGpwstoreException("No password found for url %s." %(url,))
        category, hostname, service, username = data
        self.__check_access__(pwstore_user, category)
        return self.__read_password_gpg__(category, hostname, service, username)

    def get_metadata(self, pwstore_user):
        self.__check_metadata_access__(pwstore_user)
        data = self.__get_metadata__()
        return self.__encrypt_data__(json.dumps(data), self.gpg_keys[pwstore_user])

    def commit(self):
        if self.repository.untracked_files:
            self.repository.git.add(self.repository.untracked_files)
        commit_details=""
        if self.commit_messages:
            self.commit_messages = [ '* %s' %(x,) if x else x for x in self.commit_messages ]
            commit_details='\n'.join([''] + self.commit_messages)
        self.repository.git.commit(m="monkeystore commit\n%s" %(commit_details,), a=True)
        self.commit_messages = []


    def crypt_data(self,pwstore_user, data):
        if not pwstore_user in self.users:
            raise GPGpwstoreException("Unknown user: %s" %(pwstore_user,))
        return self.__encrypt_data__(data, self.gpg_keys[pwstore_user])

    def __decrypt_with_token__(self,gpg_data, token):
        return self.__decrypt_data__(gpg_data, passphrase=token)



