# -*- coding: utf-8 -*-
__author__ = "Bernd Zeimetz"
__contact__ = "bzed@debian.org"
__license__ = """
Copyright (C) 2010 Bernd Zeimetz <bzed@debian.org>
Copyright (C) 2012 Bernd Zeimetz <b.zeimetz@conova.com>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import flask
import string
from flaskext.xmlrpc import XMLRPCHandler, Fault
from flaskext.flask_shelve import get_shelve, init_app as flask_shelve_init

from pwgen import pwgen
from .pwstore import GPGpwstore, GPGpwstoreException
app = flask.Flask(__name__)

__all__ = ["app"]

app.config.from_object('monkeystore.config')
app.debug = app.config['DEBUG']

flask_shelve_init(app)

api = XMLRPCHandler('api')
api.connect(app, '/')

import logging.handlers
app.logger.setLevel(logging.INFO)

file_handler = logging.handlers.WatchedFileHandler(app.config['LOGGER_LOCATION'])
if app.debug:
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.DEBUG)
else:
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
app.logger.addHandler(file_handler)

#log_handler = logging.handlers.SysLogHandler(address="/dev/log", facility=logging.handlers.SysLogHandler.LOG_LOCAL0)
#log_handler.setLevel(logging.DEBUG)
#app.logger.addHandler(log_handler)

gpg_pwstore = GPGpwstore(app.config)

def __take_token__(pwstore_user):
    db = get_shelve('c')
    try:
        token=db[pwstore_user]
    except Exception, e:
        raise Fault("Failed to retrieve old token", "user %s" %(pwstore_user,))
    if not token:
        raise Fault("Failed to retrieve old token", "user %s" %(pwstore_user,))
    db[pwstore_user]=None
    return token

def __put_token__(pwstore_user, token):
    db = get_shelve('c')
    db[pwstore_user] = token


@api.register
def add_password(pwstore_user,
                 category, hostname, service, username,
                 password_crypt, description_crypt=''):
    token = __take_token__(pwstore_user)
    app.logger.info("User %s adding %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
    try:
        return gpg_pwstore.add_password(pwstore_user, token,
                                   category, hostname, service, username,
                                   password_crypt, description_crypt)
    except GPGpwstoreException, e:
        app.logger.warn("User %s failed to add %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
        raise Fault("Failed to add password", str(e))

@api.register
def update_password(pwstore_user,
                    category, hostname, service, username,
                    password_crypt='', description_crypt=''):
    token = __take_token__(pwstore_user)
    update_message = []
    if password_crypt=='':
        password_crypt=None
        update_message.append('password')
    if description_crypt=='':
        description_crypt=None
        update_message.append('description')
    if len(update_message) > 0:
        app.logger.info("User %s updating %s of %s/%s/%s/%s." %(' and '.join(update_message),
                                                                pwstore_user, category,
                                                                hostname, service,username))
    try:
        gpg_pwstore.update_password(pwstore_user, token,
                                   category, hostname, service, username,
                                   password_crypt, description_crypt)
    except GPGpwstoreException, e:
        app.logger.warn("User %s failed to update %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
        raise Fault("Failed to update password", str(e))

@api.register
def reencrypt_password(pwstore_user,
                       category, hostname, service, username,
                       password_crypt):
    token = __take_token__(pwstore_user)
    app.logger.info("User %s reencrypting %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
    try:
        gpg_pwstore.update_password(pwstore_user, token,
                                   category, hostname, service, username,
                                   password_crypt, None, reencrypt=True)
    except GPGpwstoreException, e:
        app.logger.warn("User %s failed to reencrypt %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
        raise Fault("Failed to reencrypt password", str(e))

@api.register
def get_password(pwstore_user, category, hostname, service, username):
    app.logger.info("User %s retrieving %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
    try:
        return gpg_pwstore.get_password(pwstore_user, category, hostname, service, username)
    except GPGpwstoreException, e:
        app.logger.warn("User %s failed to retrieve %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
        raise Fault("Failed to retrieve password",str(e))

@api.register
def get_password_by_url(pwstore_user, url):
    app.logger.info("User %s retrieving %s." %(pwstore_user, url))
    try:
        return gpg_pwstore.get_password_by_url(pwstore_user, url)
    except GPGpwstoreException, e:
        app.logger.warn("User %s failed to retrieve %s." %(pwstore_user, url))
        raise Fault("Failed to retrieve password",str(e))


@api.register
def search(pwstore_user, *args):
    if len(args) == 1:
        category=None
        search_string_crypt=args[0]
    elif len(args) == 2:
        category, search_string_crypt = args
    else:
        raise Fault("search() takes 3 or 4 arguments (%s given)" %(str( len(args) + 1 ),))
    token = __take_token__(pwstore_user)
    try:
        search_string, data = gpg_pwstore.search(pwstore_user, token, search_string_crypt, category)
    except Exception,e:
        app.logger.warn("User %s failed to search %s." %(pwstore_user, str(e)))
        raise Fault("Search failed",str(e))
    app.logger.info("User %s searching for %s in category %s." %(pwstore_user, search_string, str(category)))
    return data


@api.register
def list_categories(pwstore_user, token):
    if token != __take_token__(pwstore_user):
        raise Fault('Failed to retrieve categories', "Token invalid - no access!")
    ret = []
    for k,v in app.config['CATEGORY_USERS'].iteritems():
        if pwstore_user in v:
            ret.append(k)
    return ret


@api.register
def generate_password(length=8):
    if length < 8:
        raise Fault('Failed to generate password', 'Minimum password length: 8')
    return pwgen(length, num_pw=1,
                 numerals=True, capitalize=True,
                 symbols=True, allowed_symbols=string.punctuation)

@api.register
def retrieve_token(pwstore_user):
    token = pwgen(200, num_pw=1, numerals=True, capitalize=True, symbols=True)
    try:
        crypted_token = gpg_pwstore.crypt_data(pwstore_user, token)
    except GPGpwstoreException, e:
        raise Fault('Failed to retrieve token', str(e))
    __put_token__(pwstore_user, token)
    return crypted_token

@api.register
def get_metadata(pwstore_user):
    app.logger.info("User %s retrieving METADATA" %(pwstore_user, ))
    return gpg_pwstore.get_metadata(pwstore_user)

@api.register
def delete_password(pwstore_user, token, category, hostname, service, username):
    app.logger.info("User %s deleting %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
    if token != __take_token__(pwstore_user):
        raise Fault('Failed to delete', "Token invalid - no access!")
    return gpg_pwstore.delete_password(pwstore_user, category, hostname, service, username)

@api.register
def get_url(pwstore_user, category, hostname, service, username):
    app.logger.info("User %s retrieving shortURL for %s/%s/%s/%s." %(pwstore_user, category, hostname, service,username))
    return gpg_pwstore.get_url(pwstore_user, category, hostname, service, username)

